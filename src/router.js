import {createRouter, createWebHistory} from 'vue-router'
import LoginPage from './components/LoginPage'
import SignupPage from './components/SignupPage'
import HomePage from './pages/HomePage'
import UserProfile from './components/UserProfile'
import ViewProfile from './components/ViewProfile'
const routes =[ 
    {
        path:'/', component: HomePage , meta:{auth:true}
    },  
    {
        path:'/login', component: LoginPage , meta:{auth:false}
    },
    {
        path:'/signup', component: SignupPage , meta:{auth:false}
    },
    {
        path:'/update-profile', component: UserProfile , meta:{auth:true}
    },
    {
        path:'/view-profile', component: ViewProfile , meta:{auth:true}
    }
    
]
const router = createRouter({
    history:createWebHistory(),
    routes
})

router.beforeEach((to,from,next)=>{ 
    if(to.meta.auth && window.localStorage.getItem('isAuthenticated') !== null ){
        next()
    }else if(to.meta.auth && !window.localStorage.getItem('isAuthenticated') !== null ){
        next('/login')
    }else if(!to.meta.auth && window.localStorage.getItem('isAuthenticated') !== null ){
        next('/')
    }else{
        next()
    }
})
export default router;