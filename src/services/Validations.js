// import router from "@/router";
async function checkEmail (email){
    // eslint-disable-next-line
    var filter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!filter.test(email)) {
        return false;
    }else{
        return true;
    }
}

export const signUpFormValidations = async(formData)=>{
    console.log(formData.multipleFiles)
    let emailValidation = await checkEmail(formData.email)
    // eslint-disable-next-line
    let phone = formData.phoneNumber.toString()

    if(!formData.name ||  !formData.email || !formData.password || !formData.phoneNumber || !formData.gender || !formData.remote_work ||!formData.country || !formData.image ||  !formData.resume || !formData.multipleFiles.length > 0){
        return {"success":false, "message": "Fill all fields before Register"}
    }else if(!emailValidation){
        return {"success":false, "message": "Invalid Email"}
    }else if(phone.length !== 10){
        return {"success":false, "message": "Phone number should be 10 digit number."}
    }
    else{
        return {"success":true, } 
    } 
}
export const loginFormValidations = async(formData)=>{
    let emailValidation = await checkEmail(formData.email)
    if(!formData.email || !formData.password ){
        return {"success":false, "message": "Fill all fields before Register"}
    }else if(!emailValidation){
        return {"success":false, "message": "Invalid Email"}
    }else{
        return {"success":true, } 
    }
}

export const updateFormValidations = async(formData)=>{
    console.log("formData>>>",formData)
    if(Object.keys(formData).length > 0){
        if(formData &&  formData.email && formData?.email !== ''){
            let emailValidation = await checkEmail(formData.email)
            if(!emailValidation){
                return {"success":false, "message": "Invalid Email"}
            }
        }
        if(formData &&  formData.phoneNumber && formData?.phoneNumber !== ""){
            if(formData.phoneNumber.length !== 10){
                return {"success":false, "message": "Phone number should be 10 digit number."}
            }
        }
        return {"success":true, } 
    }else{
        console.log("here")
        return {"success":false, "message":"No changes for update" }
        // router.push('/view-profile')
    }
}