import router from '@/router';
import {SIGNUP_ACTION,SET_USER_DATA,LOGIN_ACTION,FETCH_USER,UPDATE_USER,UPDATE_FILE} from '../../storeConstants'
import axios from 'axios';
import { useNotification } from "@kyvg/vue3-notification";

const { notify}  = useNotification()
export default{
    async [SIGNUP_ACTION](context,payload){
        // eslint-disable-next-line
        try{
            let formdata = new FormData()
            formdata.append("fullName",payload.name)
            formdata.append("email",payload.email)
            formdata.append("password",payload.password)
            formdata.append("phoneNumber",payload.phoneNumber)
            formdata.append("gender",payload.gender)
            formdata.append("profile",payload.image)
            formdata.append("resume",payload.resume)
            formdata.append("country",payload.country)
            formdata.append("radio",payload.remote_work)
            for (let i = 0; i < payload.multipleFiles.length; i++) {
                formdata.append('multiplefiles', payload.multipleFiles[i]);
            }
            let signupresp = await axios.post("http://localhost:3100/api/signup", formdata)
            if(signupresp.status == "200"){
                context.commit(SET_USER_DATA,{
                   "user":signupresp.data.user,
                })
                notify({
                    text: 'You have been registered successfully',
                    type: 'success',
                    duration: 2000,
                })
                router.push('/login')
            }
        }catch(err){
            if(err?.response?.data?.msg){
                throw err?.response?.data?.msg
            }
        }
    },
    async [LOGIN_ACTION](context,payload){
        // eslint-disable-next-line

        let loginresp = await axios.post("http://localhost:3100/api/login", {
            email:payload.email,
            password:payload.password
        })
        if(loginresp.status == "200"){
            localStorage.setItem('isAuthenticated',true)
            localStorage.setItem('token',loginresp.data.token)
            context.commit(SET_USER_DATA,{
               "user":loginresp.data.user,
               "token":loginresp.data.token
            })
            notify({
                text: 'You have been logged in!',
                type: 'success',
                duration: 2000,
            })
            router.push('/')
        }
    },
    async [FETCH_USER](context,payload){
        // eslint-disable-next-line
        try{
            let fetchUser = await axios.get("http://localhost:3100/api/fetchuser", {headers: {"x-auth-token": payload.token}})
            if(fetchUser.status == "200"){
                localStorage.setItem('isAuthenticated',true)
                localStorage.setItem('token',fetchUser.data.token)
                context.commit(SET_USER_DATA,{
                   "user":fetchUser.data.user,
                   "token":fetchUser.data.token
                })
                return fetchUser.data.user
            }
        }catch(err){
            if(err?.response?.data?.msg){
                throw err?.response?.data?.msg
            }
        }
    },
    async [UPDATE_USER](context,payload){

        // eslint-disable-next-line
        try{
            let updateUser = await axios.post("http://localhost:3100/api/updateUser", payload.updateObj,{headers: {"x-auth-token": payload.token}})
            if(updateUser.status == "200"){
                localStorage.setItem('isAuthenticated',true)
                localStorage.setItem('token',updateUser.data.token)
                context.commit(SET_USER_DATA,{
                   "user":updateUser.data.user,
                   "token":updateUser.data.token
                })
                notify({
                    text: 'Profile updated successfully',
                    type: 'success',
                    duration: 2000,
                })
                router.push('/view-profile')
            }
    
        }catch(err){
            if(err?.response?.data?.msg){
                throw err?.response?.data?.msg
            }
        }
    },
    async [UPDATE_FILE](context,payload){

        // eslint-disable-next-line
        try{
            let formdata =  new FormData()
            console.log("payload.newfile",payload.newfile)
            console.log("payload.oldfile",payload.oldfile)
            formdata.append("multiplefiles",payload.newfile)
            formdata.append("newfile",payload.oldfile)
            const updateFile = await axios.post(`http://localhost:3100/api/updatefile/${payload.oldfile}`,formdata,{headers: {"x-auth-token": payload.token}})
            if(updateFile.status == "200"){
                context.commit(SET_USER_DATA,{
                   "user":updateFile.data.user,
                   "token":updateFile.data.token
                })
                notify({
                    text: 'File Updated successfully',
                    type: 'success',
                    duration: 2000,
                })
                router.push('/view-profile')
            }
    
        }catch(err){
            if(err?.response?.data?.msg){
                throw err?.response?.data?.msg
            }
        }
    },
}
