import { IS_USER_AUTHENTICATED } from "../../storeConstants";

export default{  
    [IS_USER_AUTHENTICATED] : (state)=>{
        return !!state.token
    },

}