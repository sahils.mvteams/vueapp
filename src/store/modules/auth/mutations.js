import { SET_USER_DATA } from "../../storeConstants"
export default{
    async[SET_USER_DATA](state,payload){
        state.users = payload.user
        state.token = payload.token
    }
}