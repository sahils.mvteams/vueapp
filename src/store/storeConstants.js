export const SIGNUP_ACTION = '[actions] signup';
export const LOGIN_ACTION = '[actions] login';
export const USER_UPDATE_ACTION = '[actions] update user'
export const SET_USER_DATA = '[mutations] set user data';
export const IS_USER_AUTHENTICATED = '[getters] is user authenticated';
export const FETCH_USER = '[actions] fetch user';
export const UPDATE_USER = '[actions] update user'
export const UPDATE_FILE = '[actions] update file'